extends Window

@onready var go_to_menu_button = $CenterContainer/GoToMenuButton

func _on_go_to_menu_clicked():
	get_tree().change_scene_to_file("res://menu.tscn")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	go_to_menu_button.connect("button_down", _on_go_to_menu_clicked)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
