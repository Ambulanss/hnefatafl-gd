### Hnefatafl - godot implementation

Hello, this is my pet project I've been making to learn Godot and gdscript.
Currently it's not really playable and no builds are available.

#### Main goals:
* implement rules as defined in [simple tafl rules](http://aagenielsen.dk/simple_hnefatafl_rules.php)
* implement a mode for 2 players on one machine
* implement the possibility to play a game on several board sizes, with 7x7 (Brandubh), 9x9 (Tablut) and 11x11(Hnefatafl) as the first-class citizens


#### Known missing features:
* [x] check if the correct player's piece is moved
* [ ] win condition verification (king captured, king on edge, defenders surrounded)
* [ ] main menu
* [ ] setup for 7x7 and 11x11 board sizes
