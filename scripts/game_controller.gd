extends Node

class_name GameController

enum SquareState {
	Empty,
	King,
	Defender,
	Attacker
}

signal result_happened(result: String)

@export var black_to_move: bool = true
var BOARD: Array[Array] = [] # Array[Array[SquareState]]
@export var board_size: int = 7 :
	set (value):
		set_board(value)
		board_size = value
	get:
		return board_size

func set_board(size: int):
	if size % 2 == 0:
		return
	BOARD.clear()
	for i in range(size):
		BOARD.append([])
		for j in range(size):
			BOARD[i].append(SquareState.Empty)
	setup_pawns(size)

func setup_pawns(size: int):
	match size:
		7:
			setup_brandubh()
		9:
			setup_tablut()
		11:
			setup_hnefatafl()
		_:
			print("Unsupported board size")
			return

func setup_brandubh():
	var throne: Vector2i = Vector2i(3, 3)
	const def_squares: Array[Vector2i] = [
		Vector2i(3, 4),
		Vector2i(3, 2),
		Vector2i(2, 3),
		Vector2i(4, 3)
	]
	const attacker_squares: Array[Vector2i] = [
		Vector2i(3, 0),
		Vector2i(3, 1),
		Vector2i(3, 5),
		Vector2i(3, 6),
		Vector2i(5, 3),
		Vector2i(6, 3),
		Vector2i(0, 3),
		Vector2i(1, 3)
	]
	set_pawn_to_squares(SquareState.Defender, def_squares)
	set_pawn_to_squares(SquareState.Attacker, attacker_squares)
	set_pawn_to_squares(SquareState.King, [throne])

func setup_tablut():
	var throne = Vector2i(4, 4)
	const def_squares : Array[Vector2i] = [
		Vector2i(3, 4),
		Vector2i(2, 4),
		Vector2i(5, 4),
		Vector2i(6, 4),
		Vector2i(4, 2),
		Vector2i(4, 3),
		Vector2i(4, 5),
		Vector2i(4, 6),
	]
	const attacker_squares : Array[Vector2i] = [
		Vector2i(0, 3),
		Vector2i(0, 4),
		Vector2i(0, 5),
		Vector2i(1, 4),
		Vector2i(3, 0),
		Vector2i(3, 8),
		Vector2i(4, 0),
		Vector2i(4, 1),
		Vector2i(4, 7),
		Vector2i(4, 8),
		Vector2i(5, 0),
		Vector2i(5, 8),
		Vector2i(7, 4),
		Vector2i(8, 3),
		Vector2i(8, 4),
		Vector2i(8, 5),
	]
	set_pawn_to_squares(SquareState.Defender, def_squares)
	set_pawn_to_squares(SquareState.Attacker, attacker_squares)
	set_pawn_to_squares(SquareState.King, [throne])

func setup_hnefatafl():
	var throne = Vector2i(5, 5)
	const def_squares : Array[Vector2i] = [
		Vector2i(4, 5),
		Vector2i(3, 5),
		Vector2i(6, 5),
		Vector2i(7, 5),
		Vector2i(5, 3),
		Vector2i(5, 4),
		Vector2i(5, 6),
		Vector2i(5, 7),
	]
	const attacker_squares: Array[Vector2i] = [
		Vector2i(0, 4),
		Vector2i(0, 5),
		Vector2i(0, 6),
		Vector2i(1, 5),
		Vector2i(4, 0),
		Vector2i(4, 1),
		Vector2i(4, 7),
		#"rank5/H5",
		#"rank5/I5",
		#"rank6/A6",
		#"rank6/I6",
		#"rank8/E8",
		#"rank9/D9",
		#"rank9/E9",
		#"rank9/F9",
	]
	set_pawn_to_squares(SquareState.Defender, def_squares)
	set_pawn_to_squares(SquareState.Attacker, attacker_squares)
	set_pawn_to_squares(SquareState.King, [throne])

func set_pawn_to_squares(pawn: SquareState, squares: Array[Vector2i]):
	for square in squares:
		BOARD[square.x][square.y] = pawn

func _init():
	set_board(board_size)

func is_move_legal(from: Vector2i, to: Vector2i):
	if black_to_move == true and BOARD[from[0]][from[1]] == SquareState.Defender:
		return false
	if black_to_move == false and BOARD[from[0]][from[1]] == SquareState.Attacker:
		return false
	if BOARD[to.x][to.y] != SquareState.Empty:
		return false
	if from[0] != to[0] and from[1] != to[1]:
		return false
	if get_squares_between_squares(from, to).any(
		func is_not_empty(square: Vector2i): return BOARD[square.x][square.y] != SquareState.Empty):
			return false
	return true

func get_squares_between_squares(from: Vector2i, to: Vector2i) -> Array[Vector2i]:
	var squares : Array[Vector2i] = []
	if from.x == to.x:
		for i in range(min(from.y, to.y) + 1, max(from.y, to.y)):
			squares.append(Vector2i(from.x, i))
	else:
		for i in range(min(from.x, to.x) + 1, max(from.x, to.x)):
			squares.append(Vector2i(i, from.y))
	return squares

func is_between(value, a, b) -> bool:
	if a > b:
		return value < a and value > b
	else:
		return value < b and value > a

func make_move(from:Vector2i, to: Vector2i):
	print("Making move from: ", from, " to ", to)
	print('BEFORE')
	print(BOARD)
	var tmp = BOARD[from[0]][from[1]]
	print("Piece is: ", tmp)
	BOARD[from[0]][from[1]] = SquareState.Empty
	BOARD[to[0]][to[1]] = tmp
	print('AFTER MOVE')
	print(BOARD)
	assert(BOARD[from[0]][from[1]] == SquareState.Empty)
	assert(BOARD[to[0]][to[1]] != SquareState.Empty)
	make_captures(to)
	print('AFTER CAPTURES')
	print(BOARD)

	black_to_move = not black_to_move
	var result = get_result()
	if result != "Ongoing":
		result_happened.emit(result)
		print(get_result())

func capture_in_dir(square: Vector2i, dir: Vector2i):
	var next_square = square + dir
	var opposite_square = next_square + dir
	if not is_between(opposite_square.x, 0, board_size) or not is_between(opposite_square.y, 0, board_size):
		return
	var square_state = BOARD[square.x][square.y]
	var next_square_state = BOARD[next_square.x][next_square.y]
	var opposite_square_state = BOARD[opposite_square.x][opposite_square.y]
	var white_pieces = [SquareState.Defender, SquareState.King]
	match square_state:
		SquareState.Defender, SquareState.King:
			if white_pieces.has(opposite_square_state):
				if next_square_state == SquareState.Attacker:
					BOARD[next_square.x][next_square.y] = SquareState.Empty
		SquareState.Attacker:
			if opposite_square_state == SquareState.Attacker:
				print("Opposite square state is Attacker")
				if white_pieces.has(next_square_state):
					print("Taking: ", BOARD[next_square.x][next_square.y])
					BOARD[next_square.x][next_square.y] = SquareState.Empty

func is_king(square: SquareState): return square == SquareState.King

func is_king_captured() -> bool:
	for rank in BOARD:
		if rank.any(is_king):
			return false
	return true

func is_king_on_edge() -> bool:
	for i in range(board_size):
		print(i)
		var squares = [BOARD[i][board_size - 1], BOARD[i][0], BOARD[0][i], BOARD[board_size - 1][i]]
		if squares.any(is_king):
			return true
	return false

func get_result():
	if is_king_captured():
		return "Black won"
	if is_king_on_edge():
		return "White won"
	return "Ongoing"
	

func make_captures(square: Vector2i):
	for direction in [Vector2i(0, 1), Vector2i(0, -1), Vector2i(1, 0), Vector2i(-1, 0)]:
		capture_in_dir(square, direction)
