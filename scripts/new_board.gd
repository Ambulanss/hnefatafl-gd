extends VBoxContainer

var Square = load("res://square.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_board_size(value: int):
	if value % 2 == 0:
		return
	delete_children()
	for i in value:
		var rank = HBoxContainer.new()
		rank.size_flags_vertical = 3
		rank.alignment = BoxContainer.ALIGNMENT_END
		var rank_number = str(i + 1)
		rank.name = "rank" + str(rank_number)
		for j in value:
			var square = Square.instantiate()
			square.name = char(65 + j) + rank_number
			rank.add_child(square)
		add_child(rank)
	setup_pawns(value)
	
func delete_children():
	for node in get_children():
		remove_child(node)
		node.queue_free()
