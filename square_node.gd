extends Control

@export var is_occupied: bool = false
@onready var body = $StaticBody2D

func get_piece():
	return get_node("pawn")

# Called when the node enters the scene tree for the first time.
func _ready():
	body.modulate = Color(Color.BEIGE, 1.0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
