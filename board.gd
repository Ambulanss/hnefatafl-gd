extends VBoxContainer

class_name Board

var Square = load("res://square.tscn")
var King = load("res://king.tscn")
var Defender = load("res://pawn_def.tscn")
var Attacker = load("res://pawn_attack.tscn")

var game_controller: GameController

@export var player_to_move_label: Label
 
@export var black_to_move: bool = true :
	set (value):
		if player_to_move_label != null:
			var text = "Move: "
			if value == true:
				text += "black"
			else:
				text += "white"
			player_to_move_label.set_text(text)
		black_to_move = value
	get:
		return black_to_move

@export var board_size: int

func set_board_size(value: int):
	if value % 2 == 0:
		return
	delete_children()
	for i in value:
		var rank = HBoxContainer.new()
		rank.size_flags_vertical = 3
		rank.alignment = BoxContainer.ALIGNMENT_END
		var rank_number = str(i + 1)
		rank.name = "rank" + str(rank_number)
		for j in value:
			var square = Square.instantiate()
			square.name = char(65 + j) + rank_number
			rank.add_child(square)
		add_child(rank)
	setup_pawns(value)

func vec2iToSquarePath(coords: Vector2i) -> String:
	var format_string = "rank{rank_num}/{file_letter}{rank_num}"
	var rank_num = str(coords.x + 1)
	var file_letter = char(coords.y + 'A'.unicode_at(0))
	return format_string.format({"rank_num" : rank_num, "file_letter" : file_letter})

func update_pawns():
	for i in range(game_controller.board_size):
		for j in range(game_controller.board_size):
			print(i, j)
			var square_state = game_controller.BOARD[i][j]
			print(square_state)
			match square_state:
				GameController.SquareState.King:
					set_pawn_to_squares(King, [vec2iToSquarePath(Vector2i(i, j))])
				GameController.SquareState.Defender:
					set_pawn_to_squares(Defender, [vec2iToSquarePath(Vector2i(i, j))])
				GameController.SquareState.Attacker:
					set_pawn_to_squares(Attacker, [vec2iToSquarePath(Vector2i(i, j))])

func setup_pawns(value: int):
	match value:
		7:
			print("Board size is 7")
			custom_minimum_size.x = 480
			custom_minimum_size.y = 480
			update_pawns()
		9:
			print("Board size is 9")
			custom_minimum_size.x = 600
			custom_minimum_size.y = 600
			update_pawns()
		11:
			custom_minimum_size.x = 720
			custom_minimum_size.y = 720
			update_pawns()
		_:
			print("Unsupported board size")
			return

func set_pawn_to_squares(pawn: Resource, squares: Array):
	for square_path in squares:
		print(square_path)
		var square = get_node(square_path)
		var new_pawn = pawn.instantiate()
		new_pawn.name = "pawn"
		square.add_child(new_pawn)
		square.is_occupied = true

func delete_children():
	for node in get_children():
		if node is not Window:
			remove_child(node)
			node.queue_free()

func _on_result_happened(result):
	print("Result happened: ", result)
	var victoryScreen = $victoryScreen
	var label = $victoryScreen/CenterContainer/Label
	label.text = result
	victoryScreen.show()
	for node in get_children():
		node.propagate_call("set_process", [false])

func _init():
	game_controller = GameController.new()
	game_controller.result_happened.connect(_on_result_happened)

# Called when the node enters the scene tree for the first time.
func _ready():
	set_board_size(game_controller.board_size)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
