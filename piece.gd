extends Node2D

class_name Piece

var draggable = false
var is_inside_droppable = 0
var body_ref
var offset : Vector2
var initialPos : Vector2

@onready var SQUARES = get_tree().get_nodes_in_group("squares")
@onready var BOARD = get_tree().get_nodes_in_group("board")[0]

@onready var MOVE_SOUND = load("res://res/audio/move.ogg")
@onready var TAKE_SOUND = load("res://res/audio/take.ogg")

var piece_group_to_opponent_group = {
	"defender" : "attacker",
	"attacker" : "defender"
}

func _process(delta):
	if draggable:
		if Input.is_action_just_pressed("click"):
			initialPos = global_position
			offset = get_global_mouse_position() - global_position
			global.is_dragging = true
		if Input.is_action_pressed("click"):
			global_position = get_global_mouse_position() - offset
		elif Input.is_action_just_released("click"):
			global.is_dragging = false
			offset = get_global_mouse_position() - global_position
			var some_position = get_global_mouse_position() - offset
			var tween = get_tree().create_tween()
			var current_square = self.get_parent()
			var new_square = body_ref.get_parent()
			if is_inside_droppable > 0 and is_move_legal(current_square, new_square):
				make_move(current_square, new_square)
				tween.tween_property(self, "global_position", body_ref.global_position, 0.2)\
				.set_ease(Tween.EASE_OUT).from(some_position)
			else:
				tween.tween_property(self, "global_position", initialPos, 0.2)\
				.set_ease(Tween.EASE_OUT)

func is_move_legal(from: Control, to: Control):
	print("Checking if move is legal from: ", from, to)
	return BOARD.game_controller.is_move_legal(
		square_name_to_vec2i(from.name), square_name_to_vec2i(to.name)
	)

func make_move(from: Control, to: Control):
	print("Piece makes move from ", from, " to ", to)
	from.remove_child(self)
	from.is_occupied = false
	to.add_child(self)
	to.is_occupied = true
	# self.position = body_ref.position
	make_captures(to)
	BOARD.black_to_move = not BOARD.black_to_move
	global.is_dragging = false
	BOARD.game_controller.make_move(
		square_name_to_vec2i(from.name),
		square_name_to_vec2i(to.name))
	play_move_sound()

func square_name_to_vec2i(square_name: String) -> Vector2i:
	var x = square_name.substr(0, 1).unicode_at(0) - 'A'.unicode_at(0)
	var y = square_name.unicode_at(1) - '1'.unicode_at(0)
	return Vector2i(x, y)

func make_captures(square: Control):
	var square_file = square.name.substr(0, 1)
	var square_rank = square.name.substr(1, 1)
	var square_piece_group = self.get_groups()[0]
	var farther_neighbor_condition = func(sq: Control):
		if not sq.is_occupied:
			return false
		print(sq, "square_piece_group: ", square_piece_group, " piece_group_of_sq: ", get_piece_group_of_square(sq))
		if get_piece_group_of_square(sq) == piece_group_to_opponent_group[square_piece_group]:
			return false
		var sq_file = sq.name.substr(0, 1)
		var sq_rank = sq.name.substr(1, 1)
		var file_diff = abs(square_file.to_ascii_buffer()[0] - sq_file.to_ascii_buffer()[0])
		var rank_diff = abs(int(square_rank) - int(sq_rank))
		if sq_file == square_file and rank_diff == 2:
			return true
		return sq_rank == square_rank and file_diff == 2
	var farther_neighbors = SQUARES.filter(farther_neighbor_condition)
	for neighbor in farther_neighbors:
		print("Neighbor:", neighbor)
		var square_in_between = get_squares_between_squares(square, neighbor)[0]
		print("In between: ", square_in_between)
		if get_piece_group_of_square(square_in_between) == piece_group_to_opponent_group[square_piece_group]:
			var piece = square_in_between.get_piece()
			square_in_between.remove_child(piece)
			square_in_between.is_occupied = false
			piece.queue_free()
			play_take_sound()
		

func get_piece_group_of_square(square: Control):
	var piece = square.get_piece()
	if piece == null:
		return null
	if piece.is_in_group("attacker"):
		return "attacker"
	if piece.is_in_group("defender"):
		return "defender"

func is_between(value, a, b):
	if a > b:
		return value < a and value > b
	else:
		return value < b and value > a

func get_squares_between_squares(from: Control, to: Control):
	var from_file = from.name.substr(0, 1)
	var from_rank = from.name.substr(1, 1)
	var to_file = to.name.substr(0, 1)
	var to_rank = to.name.substr(1, 1)
	if from_file != to_file:
		var remove_squares_not_in_rank = func(square: Control):
			return square.name.substr(1, 1) == from_rank and is_between(square.name.substr(0, 1), from_file, to_file)
		var filtered_squares = SQUARES.filter(remove_squares_not_in_rank)
		return filtered_squares
	elif from_rank != to_rank:
		var remove_squares_not_in_file = func(square: Control):
			return square.name.substr(0, 1) == from_file and is_between(square.name.substr(1, 1), from_rank, to_rank)
		var filtered_squares = SQUARES.filter(remove_squares_not_in_file)
		return filtered_squares
	return []

func play_move_sound():
	var audio_player = AudioStreamPlayer.new()
	add_child(audio_player)
	audio_player.set_stream(MOVE_SOUND)
	audio_player.play()
	audio_player.connect("finished", audio_player.queue_free)

func play_take_sound():
	var audio_player = AudioStreamPlayer.new()
	add_child(audio_player)
	audio_player.set_stream(TAKE_SOUND)
	audio_player.play()
	audio_player.connect("finished", audio_player.queue_free)

func _on_area_2d_body_entered(body):
	if body.is_in_group('droppable'):
		is_inside_droppable += 1
		body_ref = body
		if global.is_dragging:
			body.modulate = Color(Color.SANDY_BROWN, 1)

func _on_area_2d_body_exited(body):
	if body.is_in_group('droppable'):
		is_inside_droppable -= 1
		body.modulate = Color(Color.BEIGE, 1)


func _on_area_2d_mouse_entered():
	var moving_group = "attacker" if BOARD.black_to_move else "defender"
	if not global.is_dragging and is_in_group(moving_group):
		draggable = true
		scale = Vector2(1.05, 1.05)


func _on_area_2d_mouse_exited():
	if not global.is_dragging:
		draggable = false
		scale = Vector2(1, 1)
